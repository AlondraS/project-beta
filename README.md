# CarCar

Team:

* Alondra - Services
* Tanner - Sales

## Running CarCar

To run CarCar the first time, follow these steps:
1. Fork this repository
2. Clone it to your local machine using the command `git clone [REPOSITORY URL GOES HERE]` Do not type the brackets!
3. Build and run the project using these docker commands in your terminal, while in the project directory. Make sure Docker is running first.
```
docker volume create beta-data
docker-compose build
docker-compose up
```

Please make sure all docker containers are up and running after running these commands. React apps often take a little bit of time to start up even after their containers are built, so be patient. Once everything is running, the application can be viewed at `http://localhost:3000/` in the browser.

**All subsequent times** you only need to run `docker-compose up` from the project directory, unless you change the code yourself.

## Design

CarCar is made up of three microservices that interact with one another as outlined in the following diagram.

![Img](/images/CarCar-Diagram.jpg)

## Inventory microservice
The inventory microservice has three models.
- Manufacturer
- VehicleModel
- Automobile
**Manufacturer model**
The Manufacturer model creats manufactures and keeps a list of manufactures that can be attached to automobile model

- Api Views for Manufacturer Model:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a Manufacturer | POST | http://localhost:8100/api/manufacturers/
| List Manufacturer | GET | http://localhost:8100/api/manufacturers/
| Show a specific Manufacturer | Get | http://localhost:8100/api/manufacturers/id/
| Update Manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete Manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

- To create a Manufacturer Model you put the following in JSON in the POST Api Views
```
{
  "name": "Volkswagon"
}
```

- If the manufacturer is succesfully created the list of manufacturer should return the following
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	]
}
```
It will automatically create the href and id

- To update the manufacturer you add the id number to the end of the PUT api view and add the following to the JSON body.
```
{
  "name": "New Name"
}
```

- To show a specific manufacturer you just need to add the id number to the end of the get api view url.

- To delete a manufacturers you just need to add the id number to the end of the delete api view url.

**VehicleModel**
The vehicle model allows you to create and list vehicles, but they are attached to a specific manufacture model.

- Api Views for Vehicle Model:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a VehicleModel | POST | http://localhost:8100/api/models/
| List VehicleModel | GET | http://localhost:8100/api/models/
| Show a specific VehicleModel | Get | http://localhost:8100/api/models/id/
| Update VehicleModel | PUT | http://localhost:8100/api/models/id/
| Delete VehicleModel | DELETE | http://localhost:8100/api/models/id/

- To create a Vehicle Model you put the following in JSON in the POST Api Views
```
{
  "name": "Alondra",
  "picture_url": "example.example.com"
  "manufacturer_id": 1
}
```

- If the Vehicle Model is succesfully created the list of manufacturer should return the following
```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Camry",
			"picture_url": "example.example.com",
			"manufacturer": {
				"href": "/api/manufacturers/9/",
				"id": 9,
				"name": "Toyota"
			}
		}
	]
}
```
It will automatically create the href and id and render the manufactuer info of the the manufacturer id that you put.

- To update the Vehicle Model you add the id number to the end of the PUT api view and add the following to the JSON body.
```
{
  "name": "New Name",
  "picture_url": "New url"
  "manufacturer_id": New Id
}
```

- To show a specific Vehicle Model you just need to add the id number to the end of the get api view url.

- To delete a Vehicle Model you just need to add the id number to the end of the delete api view url.

**Automobile**
The vheicle model creates a specific vehicle that can be connected to the Vhicle Model which can then be connected to the Manuacturer Model

- Api Views for Automobile Model:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a Automobile | POST | http://localhost:8100/api/automobile/
| List Automobile | GET | http://localhost:8100/api/automobile/
| Show a specific Automobile | Get | http://localhost:8100/api/automobile/vin/
| Update Automobile | PUT | http://localhost:8100/api/automobile/vin/
| Delete Automobile | DELETE | http://localhost:8100/api/automobile/vin/

- To create a Automobile Model you put the following in JSON in the POST Api Views
```
{
	"vin": "555AAA",
	"color": "black",
	"year": "2015",
	"model_id": 1
}
```

- If the Automobile Model is succesfully created the list of manufacturer should return the following
```
{
	"autos": [
		{
			"href": "/api/automobiles/123ABC/",
			"id": 1,
			"color": "Gray",
			"year": 2023,
			"vin": "123ABC",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Camry",
				"picture_url": "example.example.com",
				"manufacturer": {
					"href": "/api/manufacturers/9/",
					"id": 9,
					"name": "Toyota"
				}
			}	
		}		
	]
}
```

- To update the Automobile Model you add the id number to the end of the PUT api view and add the following to the JSON body.
```
{
  "color": "New Color",
  "year": New Year
}
```

- To show a specific Automobile Model you just need to add the id number to the end of the get api view url.

- To delete a Automobile Model you just need to add the id number to the end of the delete api view url.

## Service microservice
The service microservice contains three models.
- Technician
- Appointment
- AutomobileVo

**Technician model**
The Technician model creates technicians and keeps a list of technians so it can be used in the appointments model.

- Api Views for Technician Model:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a Technician | POST | http://localhost:8080/api/technicians/
| List Technician | GET | http://localhost:8080/api/technicians/
| Delete a Technician | DELETE | http://localhost:8080/api/technicians/id/

- To create a Technician Model you put the following in JSON in the POST Api Views
```
{
	"first_name": "Alondra",
	"last_name": "Solis",
	"employee_id": "100A"
}
```

- If the technician is succesfully created the list of technician should return the following
```
{
	"technicians": [
	{
	"href": "/api/technicians/1/",
	"id": 1,
	"first_name": "Alondra",
	"last_name": "Solis",
	"employee_id": "100A"
	}
	]
}
```
It will automatically create the href and id

- To delete a customer you just need to add the id number to the end of the delete api view url.

**Appointment Model**
The appointment model can create appointments so services can be conducted to a vehicle

- Api Views for Appointment Model:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a Appointment | POST | http://localhost:8080/api/appointments/
| List Appointments | GET | http://localhost:8080/api/appointments/
| Delete a Appointment | DELETE | http://localhost:8080/api/appointments/id/
| Update Appointment to Canceled | PUT | http://localhost:8080/api/appointments/id/cancel
| Update Appointment to Finished | PUT | http://localhost:8080/api/appointments/id/finish

- To create an appointment you can put the following to the JSON in the POST Api Views
```
{
	"date": "2023-7-27",
	"time": "5:15",
	"reason": "car engine",
	"vin": "6002",
	"customer": "Alondra",
	"technician": 1,
}
```

- If the appointment is succesfully created the list of appointments should return the following
```
{
	"appointments": [
		{
			"href": "/api/appointments",
			"id": 40,
			"date": "2023-07-27",
			"time": "5:15",
			"reason": "car engine",
			"vin": "555AAA",
			"customer": "Alondra",
			"is_vip": false,
			"status": "created",
			"technician": {
				"href": "/api/technicians/1/",
				"id": 1,
				"first_name": "First Technician",
				"last_name": "Last Name",
				"employee_id": "100A"
			}
		}
	]
}
```
The status is set default created and the href and and is are automatically created

- To delete a appointment you just need to add the id number to the end of the delete api view url.

_To updated status to finised or canceled you add the the id number and either cancel or finish the the put api views url. Then you add the following to JSON in the PUT Api views.
{
	"status": "canceled"
}
or for finish
{
	"status": "finished"
}


## Sales microservice

The sales microservice contains four models:
- Customer
- Salesperson
- Sale
- AutomobileVO

**The Customers model** keeps track of potential and past buyers of cars. The following API views are available for Customers.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a Customer | POST | `http://localhost:8090/api/customers/`
| List Customers | GET | `http://localhost:8090/api/customers/`
| Delete a Customer | DELETE | `http://localhost:8090/api/customers/[CUSTOMER ID]`

*Creating a Customer* requires the following information:
```
{
	"first_name": "Tanner",
	"last_name": "Jackson",
	"address": "1234 Electric Avenue, Funkytown, WA",
	"phone_number": "123-456-7890"
}
```
If successful, the API will return something like this:
```
{
	"href": "/api/customers/1/",
	"first_name": "Tanner",
	"last_name": "Jackson",
	"phone_number": "123-456-7890",
	"address": "1234 Electric Avenue, Funkytown, WA",
	"id": 1
}
```

*Listing all Customers* will supply an object with the key "customers" and a value of a list of objects, each containing a customer, like so:
```
{
	"customers": [
		{
			"href": "/api/customers/1/",
			"first_name": "Tanner",
			"last_name": "Jackson",
			"phone_number": "123-456-7890",
			"address": "1234 Electric Avenue, Funkytown, WA",
			"id": 1
		}
    ]
}
```

*Deleting a Customer* requires no body, but does require the [CUSTOMER ID] in the url be replaced with the numerical value listed as "id" brackets and all. It will return the following if successful:
```
{
	"deleted": true
}
```

**The Salesperson model** keeps track of employees on the sale floor. The following API views are available for salespeople:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a Salesperson | POST | `http://localhost:8090/api/salespeople/`
| List Salespeople | GET | `http://localhost:8090/api/salespeople/`
| Delete a Salesperson | DELETE | `http://localhost:8090/api/salespeople/[SALESPERSON ID]`

*Creating a Salesperson* requires the following information. Note that `employee_id` can be any string, it doesn't have to be firstnamelastname. Note *also* that this is separate from the id property requested by the delete method.
```
{
	"first_name": "Jane",
	"last_name": "Doe",
	"employee_id": "janedoe"
}
```
If successful, it should return something like this:
```
{
	"href": "/api/salespeople/1",
	"employee_id": "janedoe",
	"first_name": "Jane",
	"last_name": "Doe",
	"id": 1
}
```

*Listing all Salespeople* will return an object with the key "salespeople" and a value of a list of objects, each corresponding to a salesperson, like so:
```
{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"employee_id": "janedoe",
			"first_name": "Jane",
			"last_name": "Doe",
			"id": 1
		}
    ]
}
```

*Deleting a Salesperson*, as with deleting a customer, requires no body, but does require the salesperson's ID. Again, this is different than the `employee_id` property, and is a numerical value. On a success, it will return this:
```
{
	"deleted": true
}
```

**The Sale model** is a complex beast that keeps track of which salesperson sells what car to which customer. The following API calls are available for it:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a Sale | POST | `http://localhost:8090/api/sales/`
| List Sales | GET | `http://localhost:8090/api/sales/`
| List Sales by Salesperson | GET | `http://localhost:8090/api/sales/salesperson/[SALESPERSON ID]/`
| Delete a Sale | DELETE | `http://localhost:8090/api/sales/[SALE ID]/`

*Creating a Sale* requires some very specific information. Please note, the values for `vin`, `customer`, and `salesperson` all must precisely match an existing Vehicle Identification Number, customer ID, and salesperson ID respectively, and that `price` must be a number. It's probably easiest to just do this through the front-end, but the raw JSON data is as follows:
```
{
	"price": 5000,
	"automobile": "4PX900",
	"salesperson": 1,
	"customer": 1
}
```
If successful, it'll return something like this:
```
{
	"price": 5000,
	"salesperson": {
		"href": "/api/salespeople/1/",
		"employee_id": "johndoe",
		"first_name": "John",
		"last_name": "Doe",
		"id": 1
	},
	"automobile": {
		"vin": "4PX900",
		"sold": true
	},
	"customer": {
		"href": "/api/customers/1/",
		"first_name": "Jim",
		"last_name": "James",
		"phone_number": "123-456-7890",
		"address": "123 Electric Avenue",
		"id": 1
	},
	"id": 1
}
```

*Listing all Sales*, as with the others, returns an object with the key "sales" and a value of a list of objects each containing a sale, like this:
```
{
	"sales": [
		{
			"price": 5000,
			"id": 1,
			"salesperson": {
				"href": "/api/salespeople/1/",
				"employee_id": "johndoe",
				"first_name": "John",
				"last_name": "Doe",
				"id": 1
			},
			"customer": "Jim James",
			"automobile": "4PX900"
		}
    ]
}
```

*Listing Sales by Salesperson* does much the same thing, but requires the salesperson's ID.

*Deleting a Sale* requires no body but does require the sale ID. It will return the following if successful:
```
{
	"deleted": true
}
```
Note that deleting any of the models from which a sale inherits (Customer, Salesperson, AutomobileVO) will also delete the sale as it cannot exist without those fields.

**AutomobileVO** is a value object that is polled from the Inventory microservice, meaning essentially that they're simplified "copies" of the Automobile objects that exist elsewhere. Since only the VIN and the status of the car (sold or unsold) are needed by the sales microservice, this is all that the VOs contain. They are kept constantly up to date, reflecting changes made to the Automobile objects using the poller. When a new sale is made, the form allows you to choose from a list of unsold cars. Submitting the form through the front end updates the "sold" value to true. Sending a request through Insomnia manually *does not* update this value and may cause weird behavior.
