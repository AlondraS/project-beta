from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Sale, Customer

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "id",
    ]

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "employee_id",
        "first_name",
        "last_name",
        "id",
    ]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "address",
        "id",
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "phone_number",
        "address",
        "id",
    ]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "id",
        "salesperson",
    ]
    encoders = {
        "salesperson": SalespersonDetailEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "customer": f"{o.customer.first_name} {o.customer.last_name}",
            "automobile": o.automobile.vin,
        }

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
        "salesperson",
        "automobile",
        "customer",
        "id",
    ]
    encoders = {
        "salesperson": SalespersonDetailEncoder(),
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerDetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_salesperson(request, pk):
    count, _ = Salesperson.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerListEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder = CustomerDetailEncoder,
            safe = False,
        )

@require_http_methods(["DELETE"])
def api_customer(request, pk):
    count, _ = Customer.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            automobile.sold = True
            content["automobile"] = automobile
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid Salesperson ID"},
                status=400,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid Customer ID"},
                status=400,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid Automobile ID"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_list_sales_by_salesperson(request, salesperson_id):
    sales = Sale.objects.filter(salesperson__id=salesperson_id)
    return JsonResponse(
        {"sales": sales},
        encoder=SaleListEncoder,
    )

@require_http_methods(["DELETE"])
def api_sale(request, pk):
    count, _ = Sale.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET"])
def api_list_automobile_vos(request):
    auto_vos = AutomobileVO.objects.all()
    return JsonResponse(
        {"auto_vos": auto_vos},
        encoder=AutomobileVOEncoder,
    )

@require_http_methods(["DELETE"])
def api_delete_automobile_vos(request, vin):
    count, _ = AutomobileVO.objects.filter(vin=vin).delete()
    return JsonResponse(
        {"deleted": count > 0}
    )
