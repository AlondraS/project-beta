import { useState } from "react";

function AutomobileList({automobiles, getAutomobiles}) {
    const [ deleteSuccess, setDeleteSuccess ] = useState(false);


    async function handleDelete(event) {
        event.preventDefault();
        const autoUrl = `http://localhost:8100/${event.target.value}`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            getAutomobiles();
            setDeleteSuccess(true);
        }
    }

    function hideDeleteMessage(event) {
        event.preventDefault();
        setDeleteSuccess(false);
    }

    let deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (deleteSuccess) {
        deleteAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    return (
        <>
            <h1>List of Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(auto => {
                        return (
                            <tr key={auto.href}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{auto.sold ? 'Yes' : 'No'}</td>
                                <td>
                                    <button className="btn btn-secondary" onClick={handleDelete} value={auto.href}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className={deleteAlertClasses} role="alert">
                Deletion successful!
                <button type="button" className="btn-close" onClick={hideDeleteMessage} aria-label="Close"></button>
            </div>
        </>
    )

}

export default AutomobileList;
