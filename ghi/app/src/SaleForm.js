import React, { useState, useEffect } from 'react';

function SaleForm({ salespeople, customers, getSales, getAutomobiles }) {
    const [ automobileVos, setAutomobileVos ] = useState([]);
    const [ autoVin, setAutoVin ] = useState('');
    const [ customer, setCustomer ] = useState('');
    const [ salesperson, setSalesperson ] = useState('');
    const [ price, setPrice ] = useState('');
    const [ submitSuccess, setSubmitSuccess ] = useState(false);

    async function getAutomobileVos() {
        const autoUrl = "http://localhost:8090/api/automobiles/";
        const response = await fetch(autoUrl);
        if (response.ok) {
            const data = await response.json();
            setAutomobileVos(data.auto_vos);
        }
    }

    useEffect(() => {
        getAutomobileVos();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            automobile: autoVin,
            customer,
            salesperson,
            price,
        };

        const saleUrl = "http://localhost:8090/api/sales/";
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const saleResponse = await fetch(saleUrl, fetchOptions);
        if (saleResponse.ok) {
            const updateUrl = `http://localhost:8100/api/automobiles/${autoVin}/`
            const fetchUpdateOptions = {
                method: "put",
                body: JSON.stringify({sold: true}),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const updateResponse = await fetch(updateUrl, fetchUpdateOptions);
            if (updateResponse.ok) {
                setSubmitSuccess(true);
            }

            setAutoVin('');
            setCustomer('');
            setSalesperson('');
            setPrice('');

            getSales();
            getAutomobiles();
        }
    }

    function hideSubmitMessage(event) {
        event.preventDefault();
        setSubmitSuccess(false);
    }

    let submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (submitSuccess) {
        submitAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    function handleAutoVinChange(event) {
        const value=event.target.value;
        setAutoVin(value);
    }

    function handleSalespersonChange(event) {
        const value=event.target.value;
        setSalesperson(value);
    }

    function handleCustomerChange(event) {
        const value=event.target.value;
        setCustomer(value);
    }

    function handlePriceChange(event) {
        const value=event.target.value;
        setPrice(value);
    }

    return (
        <>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <h1>Record new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className='mb-3'>
                            <select onChange={handleAutoVinChange} value={autoVin} required name="auto_vin" id="auto_vin" className='form-select'>
                                <option value="">Choose a VIN</option>
                                {automobileVos.filter(auto => auto.sold === false).map(auto => {
                                    return (
                                        <option key={auto.vin} value={auto.vin}>
                                            {auto.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className='mb-3'>
                            <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className='form-select'>
                                <option value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.href} value={customer.id}>
                                            {`${customer.first_name} ${customer.last_name}`}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className='mb-3'>
                            <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className='form-select'>
                                <option value="">Choose a salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.href} value={salesperson.id}>
                                            {`${salesperson.first_name} ${salesperson.last_name}`}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handlePriceChange} value={price} required placeholder='Price' type="number" name="price" id='price' className='form-control' />
                            <label htmlFor='price'>Price</label>
                        </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
            <div className={submitAlertClasses} role="alert">
                Form submitted successfully!
                <button type="button" className='btn-close' onClick={hideSubmitMessage} aria-label="close"></button>
            </div>
        </>
    );
}

export default SaleForm;
