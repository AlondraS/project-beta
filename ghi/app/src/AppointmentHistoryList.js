import React, { useState } from "react";

function AppointmentHistoryList(props) {
    const [searchInput, setSearchInput] = useState('');

    function searchItems (event) {
        const value = event.target.value;
        setSearchInput(value)
    }
    let listAppointments = props.appointments;
    
    if (searchInput !== '') {
        listAppointments = props.appointments.filter(appointment => appointment.vin === searchInput);
    } 


    return (
      <div>
          <div>
            <input icon='search'
                placeholder='Search...'
                onChange={searchItems}
            />
          </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Vin</th>
              <th>Is VIP?</th>
              <th>Customer</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {listAppointments.map(appointment => {
              return (
                <tr key={appointment.href}>
                  <td>{ appointment.vin }</td>
                  <td>{ appointment.is_vip ? "Yes" : "No" }</td>
                  <td>{ appointment.customer }</td>
                  <td>{ appointment.date }</td>
                  <td>{ appointment.time }</td>
                  <td>{ appointment.technician.first_name }</td>
                  <td>{ appointment.reason }</td>
                  <td>{ appointment.status }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
}

export default AppointmentHistoryList
