import React, { useState } from "react";

function CustomerForm({ getCustomers }) {
    const [ firstName, setFirstName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ address, setAddress ] = useState('');
    const [ phoneNumber, setPhoneNumber ] = useState('');
    const [ submitSuccess, setSubmitSuccess ] = useState(false);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            address,
            phone_number: phoneNumber,
        };

        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const customerResponse = await fetch(customerUrl, fetchOptions);
        if (customerResponse.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');

            getCustomers();
            setSubmitSuccess(true);
        }
    }

    function hideSubmitMessage(event) {
        event.preventDefault();
        setSubmitSuccess(false);
    }

    let submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (submitSuccess) {
        submitAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    function handleFirstNameChange(event) {
        const value=event.target.value;
        setFirstName(value);
    }

    function handleLastNameChange(event) {
        const value=event.target.value;
        setLastName(value);
    }

    function handleAddressChange(event) {
        const value=event.target.value;
        setAddress(value);
    }

    function handlePhoneNumberChange(event) {
        const value=event.target.value;
        setPhoneNumber(value);
    }

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <h1>Add a customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={firstName} required placeholder="First Name" type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} value={lastName} required placeholder="Last Name" type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} value={address} required placeholder="Address" type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhoneNumberChange} value={phoneNumber} required placeholder="Phone Number" type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
            <div className={submitAlertClasses} role="alert">
                Form submitted successfully!
                <button type="button" className='btn-close' onClick={hideSubmitMessage} aria-label="close"></button>
            </div>
        </>
    )
}

export default CustomerForm;
