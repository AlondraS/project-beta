import { useState } from "react";

function SalespeopleList({ salespeople, getSalespeople }) {
    const [ deleteSuccess, setDeleteSuccess ] = useState(false);


    async function handleDelete(event) {
        event.preventDefault();
        const salespersonUrl = `http://localhost:8090/${event.target.value}`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            getSalespeople();
            setDeleteSuccess(true);
        }
    }

    function hideDeleteMessage(event) {
        event.preventDefault();
        setDeleteSuccess(false);
    }

    let deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (deleteSuccess) {
        deleteAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    return (
        <>
            <h1>List of Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.href}>
                                <td>{salesperson.employee_id}</td>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                                <td>
                                    <button className="btn btn-secondary" onClick={handleDelete} value={salesperson.href}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className={deleteAlertClasses} role="alert">
                Deletion successful!
                <button type="button" className="btn-close" onClick={hideDeleteMessage} aria-label="Close"></button>
            </div>
        </>
    )
}

export default SalespeopleList;
