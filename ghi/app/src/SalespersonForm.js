import React, { useState } from 'react';

function SalespersonForm({ getSalespeople }) {
    const [ firstName, setFirstName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ employeeId, setEmployeeId ] = useState('');
    const [ submitSuccess, setSubmitSuccess ] = useState(false);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        };

        const salespeopleUrl = "http://localhost:8090/api/salespeople/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const salespersonResponse = await fetch(salespeopleUrl, fetchOptions);
        if (salespersonResponse.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            setSubmitSuccess(true);

            getSalespeople();
        }
    }

    function hideSubmitMessage(event) {
        event.preventDefault();
        setSubmitSuccess(false);
    }

    let submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (submitSuccess) {
        submitAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    function handleFirstNameChange(event) {
        const value=event.target.value;
        setFirstName(value);
    }

    function handleLastNameChange(event) {
        const value=event.target.value;
        setLastName(value);
    }

    function handleEmployeeIdChange(event) {
        const value=event.target.value;
        setEmployeeId(value);
    }

    return (
        <>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <h1>Add a salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className='form-floating mb-3'>
                            <input onChange={handleFirstNameChange} value={firstName} required placeholder='First Name' type="text" name="first_name" id="first_name" className='form-control' />
                            <label htmlFor='first_name'>First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleLastNameChange} value={lastName} required placeholder='Last Name' type='text' name='last_name' id='last_name' className='form-control' />
                            <label htmlFor='last_name'>Last Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleEmployeeIdChange} value={employeeId} required placeholder='Employee ID' type='text' name='employee_id' id='employee_id' className='form-control' />
                            <label htmlFor='employee_id'>Employee ID</label>
                        </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
            <div className={submitAlertClasses} role="alert">
                Form submitted successfully!
                <button type="button" className='btn-close' onClick={hideSubmitMessage} aria-label="close"></button>
            </div>
        </>
    )
}

export default SalespersonForm;
