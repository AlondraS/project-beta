import React, { useState, useEffect } from 'react';

function VehicleModelForm({ getVehicleModels }) {
    const [ name, setName ] = useState('');
    const [ pictureUrl, setPictureUrl ] = useState('');
    const [ manufacturer, setManufacturer ] = useState('');
    const [ manufacturers, setManufacturers ] = useState([]);
    const [ submitSuccess, setSubmitSuccess ] = useState(false);

    useEffect(() => {
        async function getManufacturers() {
            const url ="http://localhost:8100/api/manufacturers/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            }
        }
        getManufacturers();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name,
            picture_url: pictureUrl,
            manufacturer_id: manufacturer,
        };

        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const modelResponse = await fetch(modelUrl, fetchOptions);
        if (modelResponse.ok) {
            setName('');
            setPictureUrl('');
            setManufacturer('');
            setSubmitSuccess(true);

            getVehicleModels();
        }
    }

    function hideSubmitMessage(event) {
        event.preventDefault();
        setSubmitSuccess(false);
    }

    let submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (submitSuccess) {
        submitAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    function handleNameChange(event) {
        const value=event.target.value;
        setName(value);
    }

    function handlePictureUrlChange(event) {
        const value=event.target.value;
        setPictureUrl(value);
    }

    function handleManufacturerChange(event) {
        const value=event.target.value;
        setManufacturer(value);
    }

    return (
        <>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <h1>Add a vehicle model</h1>
                    <form onSubmit={handleSubmit} id='create-model-form'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleNameChange} value={name} required placeholder='Name' type='text' name='name' id='name' className='form-control' />
                            <label htmlFor='name'>Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handlePictureUrlChange} value={pictureUrl} required placeholder='Picture URL' type='url' name='picture_url' id='picture_url' className='form-control' />
                            <label htmlFor='picture_url'>Picture URL</label>
                        </div>
                        <div className='mb-3'>
                            <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className='form-select'>
                                <option value="">Choose a manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.href} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
            <div className={submitAlertClasses} role="alert">
                Form submitted successfully!
                <button type="button" className='btn-close' onClick={hideSubmitMessage} aria-label="close"></button>
            </div>
        </>
    )
}

export default VehicleModelForm;
