import { useState } from "react";

function VehicleModelList({vehicleModels, getVehicleModels}) {
    const [ deleteSuccess, setDeleteSuccess ] = useState(false);

    async function handleDelete(event) {
        event.preventDefault();
        const modelUrl = `http://localhost:8100/${event.target.value}`;
        const fetchConfig = {
            method: "delete",
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            getVehicleModels();
            setDeleteSuccess(true);
        }
    }

    function hideDeleteMessage(event) {
        event.preventDefault();
        setDeleteSuccess(false);
    }

    let deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (deleteSuccess) {
        deleteAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        deleteAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    return (
        <>
            <h1>List of Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicleModels.map(model => {
                        return (
                            <tr key={model.href}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} className="img-thumbnail w-25" alt={`A car of the model ${model.name}.`}/></td>
                                <td>
                                    <button className="btn btn-secondary" onClick={handleDelete} value={model.href}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div className={deleteAlertClasses} role="alert">
                Deletion successful!
                <button type="button" className="btn-close" onClick={hideDeleteMessage} aria-label="Close"></button>
            </div>
        </>
    );
}

export default VehicleModelList;
