function AppointmentList({appointments, getAppointments }) {

  async function updateStatusCanceled(event) {
    event.preventDefault();
    const url = `http://localhost:8080/api/appointments/${event.target.value}/cancel`;
    const fetchUpdateOptions = {
      method: "put",
      body: JSON.stringify({status: 'Canceled'}),
      headers: {
          'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchUpdateOptions);
    if (response.ok) {
      getAppointments();
    }
  }

  async function updateStatusFinished(event) {
    event.preventDefault();
    const url = `http://localhost:8080/api/appointments/${event.target.value}/finish`;
    const fetchUpdateOptions = {
      method: "put",
      body: JSON.stringify({status: 'Finished'}),
      headers: {
          'Content-Type': 'application/json',
      },

    };

    const response = await fetch(url, fetchUpdateOptions);
    if (response.ok) {
      getAppointments();
    }
  }

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Vin</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {appointments.filter(appointment => appointment.status === "created").map(appointment => {
          return (
            <tr key={appointment.href}>
              <td>{ appointment.vin }</td>
              <td>{ appointment.is_vip ? 'Yes' : 'No' }</td>
              <td>{ appointment.customer }</td>
              <td>{ appointment.date }</td>
              <td>{ appointment.time }</td>
              <td>{ appointment.technician.first_name }</td>
              <td>{ appointment.reason }</td>
              <td>
              <button className="btn btn-primary" onClick={updateStatusCanceled} value={appointment.id}>
                Cancel
              </button>
              <button className="btn btn-primary" onClick={updateStatusFinished} value={appointment.id}>
                Finished
              </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    
  );
}

export default AppointmentList;
