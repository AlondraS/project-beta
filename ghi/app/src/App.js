import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useEffect, useState } from 'react';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistoryList from './AppointmentHistoryList';
import SaleList from './SaleList';
import SaleBySalespersonList from './SaleBySalespersonList';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SaleForm from './SaleForm';


function App() {
  const [ vehicleModels, setVehicleModels ] = useState([]);
  const [ automobiles, setAutomobiles ] = useState([]);
  const [ manufacturers, setManufacturers ] = useState([]);
  const [ technicians, setTechnicians ] = useState([]);
  const [ appointments, setAppointments ] = useState([]);
  const [ salespeople, setSalespeople ] = useState([]);
  const [ customers, setCustomers ] = useState([]);
  const [ sales, setSales ] = useState([]);

  async function getVehicleModels() {
    const modelUrl = "http://localhost:8100/api/models/";
    const response = await fetch(modelUrl);
    if (response.ok) {
        const data = await response.json();
        setVehicleModels(data.models);
    }
  }

  async function getAutomobiles() {
    const autoUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(autoUrl);
    if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
    }
  }


   async function getManufacturers() {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  async function getTechnicians() {
    const technicianUrl = "http://localhost:8080/api/technicians/";
    const response = await fetch(technicianUrl);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  async function getAppointments() {
    const appointmentlUrl = "http://localhost:8080/api/appointments/";
    const response = await fetch(appointmentlUrl);
    if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
    }
  }

  async function getSalespeople() {
    const salespeopleUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salespeopleUrl);
    if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
    }
  }

  async function getCustomers() {
    const customersUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customersUrl);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  async function getSales() {
    const salesUrl = "http://localhost:8090/api/sales/";
    const response = await fetch(salesUrl);
    if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
    }
  }

  useEffect(() => {
    getManufacturers();
    getVehicleModels();
    getAutomobiles();
    getTechnicians();
    getAppointments();
    getSalespeople();
    getCustomers();
    getSales();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models">
            <Route index element={<VehicleModelList vehicleModels={vehicleModels} getVehicleModels={getVehicleModels} />} />
            <Route path="create" element={<VehicleModelForm getVehicleModels={getVehicleModels} />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList automobiles={automobiles} getAutomobiles={getAutomobiles} />} />
            <Route path="create" element={<AutomobileForm getAutomobiles={getAutomobiles} vehicleModels={vehicleModels} />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
            <Route path="create" element={<ManufacturerForm manufacturers={manufacturers} getManufacturers={getManufacturers} />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList technicians={technicians} getTechnicians={getTechnicians} />} />
            <Route path="create" element={<TechnicianForm technicians={technicians} getTechnicians={getTechnicians} />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList appointments={appointments} getAppointments={getAppointments} />} />
            <Route path="create" element={<AppointmentForm getAppointments={getAppointments} />} />
            <Route path="history" element={<AppointmentHistoryList appointments={appointments} getAppointments={getAppointments} />} />
          </Route>
          <Route path="sales">
            <Route index element={<SaleList sales={sales} getSales={getSales} />} />
            <Route path="salesperson" element={<SaleBySalespersonList salespeople={salespeople} />} />
            <Route path="create" element={<SaleForm salespeople={salespeople} customers={customers} getSales={getSales} getAutomobiles={getAutomobiles} />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList salespeople={salespeople} getSalespeople={getSalespeople} />} />
            <Route path='create' element={<SalespersonForm getSalespeople={getSalespeople} />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList customers={customers} getCustomers={getCustomers} />} />
            <Route path="create" element={<CustomerForm getCustomers={getCustomers} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
