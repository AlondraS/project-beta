function ManufacturerList(props) {
    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer Name</th>
          </tr>
        </thead>
        <tbody>
          {props.manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.href}>
                {/* <td>{ manufacturer.id }</td> */}
                <td>{ manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default ManufacturerList;
