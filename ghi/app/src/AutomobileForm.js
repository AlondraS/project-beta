import React, { useState } from 'react';

function AutomobileForm({ getAutomobiles, vehicleModels }) {
    const [ vin, setVin ] = useState('');
    const [ year, setYear ] = useState('');
    const [ color, setColor ] = useState('');
    const [ model, setModel ] = useState('');
    const [ submitSuccess, setSubmitSuccess ] = useState(false);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            vin,
            year,
            color,
            model_id: model,
        };

        const autoUrl="http://localhost:8100/api/automobiles/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const autoResponse = await fetch(autoUrl, fetchOptions);
        if (autoResponse.ok) {
            setVin('');
            setYear('');
            setColor('');
            setModel('');

            getAutomobiles();
            setSubmitSuccess(true);
        }
    }

    function hideSubmitMessage(event) {
        event.preventDefault();
        setSubmitSuccess(false);
    }

    let submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    if (submitSuccess) {
        submitAlertClasses = 'alert alert-dismissible fade show alert-success';
    } else {
        submitAlertClasses = 'alert alert-success alert-dismissible fade show d-none';
    }

    function handleVinChange(event) {
        const value=event.target.value;
        setVin(value);
    }

    function handleYearChange(event) {
        const value=event.target.value;
        setYear(value);
    }

    function handleColorChange(event) {
        const value=event.target.value;
        setColor(value);
    }

    function handleModelChange(event) {
        const value=event.target.value;
        setModel(value);
    }

    return (
        <>
            <div className='row'>
                <div className='offset-3 col-6'>
                    <h1>Add an automobile</h1>
                    <form onSubmit={handleSubmit} id="create-auto-form">
                        <div className='form-floating mb-3'>
                            <input onChange={handleVinChange} value={vin} required placeholder='VIN' type='text' name='vin' id='vin' className='form-control' />
                            <label htmlFor='vin'>VIN</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleYearChange} value={year} required placeholder='Year' type='number' name='year' id='year' className='form-control' />
                            <label htmlFor='year'>Year</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleColorChange} value={color} required placeholder='Color' type='text' name='color' id='color' className='form-control' />
                            <label htmlFor='color'>Color</label>
                        </div>
                        <div className='mb-3'>
                            <select onChange={handleModelChange} value={model} required name='model' id='model' className='form-select'>
                                <option value="">Choose a model</option>
                                {vehicleModels.map(model => {
                                    return (
                                        <option key={model.href} value={model.id}>
                                            {model.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className='btn btn-primary'>Add</button>
                    </form>
                </div>
            </div>
            <div className={submitAlertClasses} role="alert">
                Form submitted successfully!
                <button type="button" className='btn-close' onClick={hideSubmitMessage} aria-label="close"></button>
            </div>
        </>
    )
}

export default AutomobileForm;
