from django.db import models
from django.urls import reverse


# class Status(models.Model):
#     name = models.CharField(max_length=10, unique=True)
#     id = models.PositiveSmallIntegerField(primary_key=True, null=False)

#     def __str__(self):
#         return self.name


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

    def __str__(self):
        return self.first_name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date = models.DateTimeField()
    time = models.CharField(max_length=100)
    reason = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)
    status = models.CharField(auto_created=True, max_length=100, default="created")
    customer = models.CharField(max_length=200)
    is_vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointments", kwargs={"pk": self.id})

    def __str__(self):
        return self.customer
