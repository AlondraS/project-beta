from django.urls import path
from .views import api_technicians_list, api_technician, api_appoinment_list, api_automobile, api_appointments


urlpatterns = [
    path("automobiles/", api_automobile, name="api_automobile"),
    path("appointments/<int:pk>/", api_appointments, name='api_appointments'),
    path("appointments/<int:pk>/finish", api_appointments, name='api_appointments'),
    path("appointments/<int:pk>/cancel", api_appointments, name='api_appointments'),
    path("appointments/history", api_appoinment_list, name='api_appoinment_list'),
    path("appointments/", api_appoinment_list, name='api_appoinment_list'),
    path("technicians/", api_technicians_list, name='api_technicians_list'),
    path("technicians/<int:pk>/", api_technician, name="api_technician")
]
