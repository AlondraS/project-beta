# Generated by Django 4.0.3 on 2023-06-08 23:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0006_alter_appointment_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='is_vip',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='status',
            field=models.CharField(auto_created=True, default='created', max_length=100),
        ),
    ]
